forever
===

syntax : forever \<command\>

sample : forever arp -an 192.168.0.1 (linux)

sample : forever arp -a 192.168.0.1 (windows)

[Click here to see forever demo mp4](forever-demo.mp4)
