set COMMAND=%*
if "%COMMAND%"=="" goto usage

:loop
echo %TIME%
%COMMAND%
timeout /t 1 > nul
goto loop

:usage
	echo "syntax : forever <command>"
	echo "sample : forever arp -a 192.168.0.1"
	
:eof